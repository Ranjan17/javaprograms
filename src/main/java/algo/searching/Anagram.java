package algo.searching;

import java.util.*;

public class Anagram {

    //Eat      ate

    public static boolean isAnagram(String s1, String s2){
        if(s1.length() !=s2.length() || s1.isEmpty() || s2.isEmpty()){
            return false;
        }
        boolean flag= true;
        char[] ch1= s1.toCharArray();
        char[] ch2= s2.toCharArray();

        Arrays.sort(ch1);
        Arrays.sort(ch2);

        if (!Arrays.equals(ch1, ch2)){
            flag=false;
        }
        return flag;
    }

   // Input  --> {"EAT","TEA","ATE","BAT","TAB","ALL"}
   // Input  --> {"EAT","TEA","ATE","BAT","TAB","ALL"}
   // Output --> {"EAT","TEA","ATE},{"BAT","TAB"},{"ALL"}

    public static List<Set<String>> groupingAnagram(List<String> al){
        Set<String> alist= new HashSet<>();
        List<Set<String>> result= new ArrayList<>();
        for (int i=0; i<al.size(); i++){
            alist.add(al.get(i));
            for (int j=i+1; j<al.size();j++){
               if( isAnagram(al.get(i),al.get(j))){
                   alist.add(al.get(j));
               }
            }
            result.add(alist);

        }
        return result;
    }

    public static void main(String[] args) {
        //System.out.println( isAnagram("eat", "atee"));
        List<String> arrayList=  Arrays.asList("EAT","TEA","ATE","BAT","TAB","ALL");
        System.out.println(groupingAnagram(arrayList));


    }
}
