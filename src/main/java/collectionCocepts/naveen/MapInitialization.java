package collectionCocepts.naveen;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapInitialization {
    public static void main(String[] args) {
        HashMap<String, String> map1 = new HashMap<>();
        Map<String, String> map2 = new HashMap<>();

        //Immutable map with one single entry
        Map<String, Integer> map3 = Collections.singletonMap("Ram", 100);
        System.out.println(map3.get("Ram"));
        //map3.put("Shyam", 200); //UnsupportedOperationException

        //JDK 8

        Map<String, String> map4 = Stream.of(new String [][]{
                {"Tom", "A",},
                {"John", "B"}
        }).collect(Collectors.toMap(data-> data[0], data->data[1]));
    }
}
