package stringProgram;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateChar {
    public static void main(String[] args) {
        printDuplicateChar("abcdefgh");
    }

    public static void printDuplicateChar(String s){
        if (s==null){
            System.out.println("Null string");
            return;
        }
        if (s.length()==1){
            System.out.println("Single character string");
            return;
        }
        if (s.isEmpty()){
            System.out.println("Empty string");
            return;
        }
        Map<Character, Integer> charMap= new HashMap<>();
        for (int i=0; i<s.length();i++){
            char c= s.charAt(i);
            if (charMap.containsKey(c)){
                charMap.put(c, charMap.get(c)+1);
            }else {
                charMap.put(c,1);
            }
        }

        Set<Map.Entry<Character,Integer>> entrySet= charMap.entrySet();
        for (Map.Entry<Character, Integer> entry:entrySet){
            if (entry.getValue()>1){
                System.out.println(entry.getKey() +":"+entry.getValue());
            }
        }
    }
}
