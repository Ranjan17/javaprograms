package collectionCocepts.naveen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArrayListCompare {
    public static void main(String[] args) {
        //1.Sort and then compare
        ArrayList<String> al1 = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "F"));
        ArrayList<String> al2 = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "E"));
        ArrayList<String> al3 = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "F"));

        Collections.sort(al1);
        Collections.sort(al2);
        Collections.sort(al3);
        System.out.println(al1.equals(al2));
        System.out.println(al1.equals(al3));
    }

}
