package stringProgram;

public class Pallindrome {

    public static boolean isPallindrome(String s){
        char [] ch= s.toCharArray();
        int start=0;
        int end= ch.length-1;
        while (start<end){
            if (ch[start]==ch[end]){
                start++;
                end--;
            }else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPallindrome("abab"));
    }
}
