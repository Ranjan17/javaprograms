package stringProgram;

import java.util.ArrayList;
import java.util.List;

public class FizzBuzz {

    public static List<Object> fizzBuzz(int n){
        List<Object> l= new ArrayList<>();
        for (int i=1;i<n; i++){
            if (i%5==0 && i%3==0){
                l.add("FizzBuzz");
            }
            else if (i%3==0){
                l.add("Fizz");
            }
            else if (i%5==0){
                l.add("Buzz");
            }else{
                l.add(i);
            }

        }
        return l;
    }

    public static void main(String[] args) {
        System.out.println(fizzBuzz(20));
    }
}
