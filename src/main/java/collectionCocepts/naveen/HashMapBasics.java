package collectionCocepts.naveen;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMapBasics {
    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("Odisha", "Bhubaneswar");
        map.put("Maharstra", "Mumbai");
        map.put("Asham", "Shilong");
        map.put("Poduchery", "Poduchery");

        System.out.println(map.get("Odisha"));

        //Iterate over all the Keys

        for (String key : map.keySet()) {
            System.out.println(map.get(key));

        }

        //Iterate over the pair

        for (Map.Entry<String, String> entry : map.entrySet()) {
            System.out.println("Key=" + entry.getKey() + "  Value=" + entry.getValue());
        }

        //Itearte hasm map using java 8
        map.forEach((k, v)-> System.out.println("Key ="+k + "  Value="+v));
    }
}
