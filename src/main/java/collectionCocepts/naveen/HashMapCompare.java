package collectionCocepts.naveen;

import java.util.HashMap;

public class HashMapCompare {
    public static void main(String[] args) {
        HashMap<Integer, String> hm1 = new HashMap<>();
        hm1.put(1,"A");
        hm1.put(2,"B");
        hm1.put(3,"C");

        HashMap<Integer, String> hm2 = new HashMap<>();
        hm1.put(3,"C");
        hm1.put(2,"B");
        hm1.put(1,"A");

        HashMap<Integer, String> hm3 = new HashMap<>();
        hm1.put(1,"A");
        hm1.put(2,"B");
        hm1.put(3,"C");
        hm1.put(3,"D");

        System.out.println(hm1.equals(hm2));
        System.out.println(hm1.equals(hm2));
    }
}
