package algo.searching;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Demo {

    /**
     * List T = {“MRashmi26”, “MAzam28”, “FPooja23”}
     *
     * List A = { M, Rashmi , 26}
     * List B = {M, Azam, 28}
     * List C ={F, Pooja, 23}
     */

    public static List<List<String>> test(List<String> al){
      //  List<String> partitionList= new ArrayList<>();
        List<List<String>> result= new ArrayList<>();
        for (String s : al){
            result.add(splitString(s));



        }
        return result;
    }

    public static List<String> splitString(String str){
        List<String> splitList= new ArrayList<>();
        //char[] ch= str.toCharArray();
       // for (int i=0; i<ch.length; i++){
            splitList.add(str.substring(0,1));
            splitList.add(str.substring(1,str.length()-2));
            splitList.add(str.substring(str.length()-2, str.length()));

            return splitList;

        }

    public static void main(String[] args) {
        List<String> st= Arrays.asList("MRashmi26", "MAzam28", "FPooja23");
        System.out.println( test(st));
    }

}




