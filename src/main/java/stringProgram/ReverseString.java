package stringProgram;

public class ReverseString {

    public static void reverseString(char[] s){
        int start=0;
        int end= s.length-1;

        while (start<end){
            char ch= s[start];
            s[start]=s[end];
            s[end]=ch;
            start++;
            end--;
        }
    }

    public static String reverseString(String s){
        char[] chars= s.toCharArray();
        int start=0;
        int end=chars.length-1;
        while (start<end){
            char ch= chars[start];
            chars[start]=chars[end];
            chars[end]=ch;
            start++;
            end--;

        }
        return String.valueOf(chars);
    }

    public static void main(String[] args) {
        System.out.println(reverseString("abcdef"));
    }
}
