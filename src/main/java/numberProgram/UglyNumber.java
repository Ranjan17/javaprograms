package numberProgram;

public class UglyNumber {

    public static boolean isUglyNumber(int num){
        int flag=0;
        if (num<=0){
            return false;
        }
        while (num!=1){
            if (num%5==0){
                num/=5;
            }else if (num%3==0){
                num/=3;
            }else if(num%2==0){
                num/=2;
            }else {
                flag=1;
                return false;
            }
        }
        return true;
    }


    public static void main(String[] args) {
        System.out.println(isUglyNumber(18));
    }
}
