package logicalProgram.stringProgram;

public class WordCount {

    public static int countOfWords(String str){
        String [] stringArray = str.trim().split(" ");
        return stringArray.length;
    }

    public static int countWords(String str){
        int count =0;
        for (int i =0; i< str.length(); i++){
            if(str.charAt(i) == ' ' & str.charAt(i+1) !=' '){
                count++;
            }
        }
        return  count;
    }
}
