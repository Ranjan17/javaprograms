package exceptionHandling;

public class Example {
    public static void main(String[] args) {
        System.out.println(m1());
    }
    static int m1(){
        try {
            try{
                System.out.println("In try");
                System.out.println(10/0);
                // return 10;
            }catch (NullPointerException n){
                n.printStackTrace();
            }finally {
                System.out.println("Inner finally");
                return 10;
            }

        }
        catch (NullPointerException ae){
            System.out.println("In catch");
            return 20;
        }finally {
            System.out.println("In finally");
            return 30;
        }


    }
}
