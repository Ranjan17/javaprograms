package java8;

import java.util.*;
import java.util.stream.Collectors;

public class SortHashMapByValues {

    public static HashMap<String, Integer> sortByValue(HashMap<String, Integer> hm){
        //Create a list from elements of HashMap
        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(hm.entrySet());
        //Sort the list
        list.sort(new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        //Put the sorted map in temp hasmap
        HashMap<String, Integer> temp = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> aa : list){
            temp.put(aa.getKey(), aa.getValue());
        }
        return  temp;
    }

    public static HashMap<String, Integer> sortByValueLambda(HashMap<String, Integer> hm){
        List<Map.Entry<String, Integer>> list
                = new LinkedList<Map.Entry<String, Integer>>(hm.entrySet());

        //Sort the list using lambda
        list.sort((i1,
                   i2) -> i1.getValue().compareTo(i2.getValue()));

        //Put data from sorted list to hashmap
        HashMap<String, Integer> temp =
                new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> aa : list){
            temp.put(aa.getKey(), aa.getValue());
        }
        return temp;

    }

    // Using streams

    public static HashMap<String, Integer> sortByValueStreams(HashMap<String, Integer> hm){
        HashMap<String, Integer> temp =
                hm.entrySet()
                .stream()
                .sorted((i1, i2)->
                        i1.getValue().compareTo(i2.getValue()))
                .collect(Collectors.toMap(
                        Map.Entry :: getKey,
                        Map.Entry :: getValue,
                        (e1, e2)->e1, LinkedHashMap::new
                ));
        return temp;
    }


    public static void main(String[] args) {
        //Creating a Hash map
        Map<String,Integer> map = new HashMap<>();
        map.put("Hari", 35);
        map.put("Nari", 30);
        map.put("Ram", 50);
        map.put("Shyam", 45);

        Map<String, Integer> sortByValue = map.entrySet().stream().sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue(),
                        (entry1, entry2)-> entry2, LinkedHashMap::new));


        System.out.println("Before sorting value "+map);
        System.out.println("After sorting value "+sortByValue);
    }
}
