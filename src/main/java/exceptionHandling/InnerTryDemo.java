package exceptionHandling;

public class InnerTryDemo {
    public static void main(String[] args) {
        try{
            System.out.println("In outer try");
            try{
                System.out.println("In inner try");
                System.out.println(10/0);
            }catch (NumberFormatException ae){
                System.out.println("inner catch");
            }
            System.out.println("Afetr inner try catch");
        }catch (NumberFormatException e){
            System.out.println("Outer catch");
        }
        System.out.println("After try catch");
    }
}
