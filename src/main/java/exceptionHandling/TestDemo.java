package exceptionHandling;

public class TestDemo {
    public static void main(String[] args) {
        try {
            System.out.println("In try");
            int c = 10/0;
        }catch (NumberFormatException ae){
            System.out.println("in catch");
        }
        finally {
            System.out.println("In finaly");
        }
        System.out.println("After try/ctch/finally");
    }
}