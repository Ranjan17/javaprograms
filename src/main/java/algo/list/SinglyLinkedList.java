package algo.list;


import java.util.HashSet;

class Node<T>{
    T data;
    Node<T> next;
    Node(T data){
        this.data=data;
    }

    Node(T data, Node<T> next){
        this.data=data;
        this.next=next;
    }
}
public class SinglyLinkedList {

    private Node<Integer> head;
    private static  int size;

    //Initialize LinkedList
    public SinglyLinkedList(){
        head=null;
        size=0;
    }

    //Init LL with given head and size
    public SinglyLinkedList(Node<Integer> head, int size){
        this.head=head;
        this.size=size;
    }

    //Insert element at head of the List
    public  void insertHead(int data){
        //To be implemented
        insertNth(data,0);
    }

    //Insert element at tail of the LL

    public  void insertTail(int data){
        //To be implemeneted
        insertNth(data,size);
    }

    //Insert new node at specific position

    public  void insertNth(int data, int position){
        checkBounds(position,0,size);
        Node<Integer> newNode=new Node<>(data);
        if (head==null){
            head=newNode;
            size++;
        }
        else if (position==0){
            newNode.next=head;
            head=newNode;
            size++;
        }
        Node<Integer> temp=head;
        for (int i = 0; i < position-1; i++) {
            temp=temp.next;
        }
        newNode.next= temp.next;
        temp.next= newNode;
        size++;
    }

    public void deleteNth(int position){
        checkBounds(position,0, size-1);
        if (position==0){
            Node<Integer> targetNode= head;
            head=head.next;
            targetNode=null;
            size--;
        }
        Node<Integer> current= head;
        for (int i = 0; i < position-1; i++) {
            current=current.next;
        }
        Node<Integer> targetNode= current.next;
        current.next=current.next.next;
        targetNode=null;
        size--;
    }

    //Delete particular key
    public void deleteKey(int key){
        Node<Integer> temp= head, prev=null;
        //If head node contains the key
        if (temp !=null && temp.data==key){
            head=temp.next; //Change the head to next node
        }
        //Search for the key to be deleted
        while (temp!=null && temp.data!=key){
            prev=temp;
            temp=temp.next;
        }

        if (temp==null || temp.next==null){
          return;
        }
        prev.next=temp.next.next;
    }


    //Delete the node at given position
    void deleteNode(int position){
        Node<Integer> temp= head,targetNode=null;
        if (position==0){
            head=temp.next;
        }
        //Find previous node of the node to be deleted
        for (int i = 0;temp!=null && i <position-1 ; i++) {
            temp=temp.next;
        }
        //If position is more than number of nodes
        if (temp==null || temp.next==null){
            return;
        }
        targetNode= temp.next.next;
        temp.next= targetNode;
    }

    public int getCount(Node<Integer> head){
        Node<Integer> temp= head;
        int count=0;
        if (temp !=null){
            count++;
            temp=temp.next;
        }
        return count;
    }

    public boolean search(Node<Integer> head, int x){
        Node<Integer> current= head;
        while (current!=null){
            if (current.data==x){
                return true;
            }
            current=current.next;
        }
        return false;
    }

    public boolean searchRecursive(Node<Integer> head, int x){
        //Base case
            if (head ==null){
                return false;
            }
            if (head.data==x){
                return true;
            }
            return searchRecursive(head.next,x);


    }

    public int getNth(int index){
        Node<Integer> current= head;
        int count=0;
        while (current !=null){
            if (count==index)
                return current.data;
            count++;
            current=current.next;
        }
        assert (false);
        return -1;
    }

    //Get Nth Node from the last of the Linked List
    public void getNthFromLast(Node<Integer> head, int n){
        Node<Integer> temp= head;
        int len=0;
        while (temp !=null){
            len++;
            temp=temp.next;
        }
        if (len<n){
            System.out.println("Given index value is greater than the length");
        }
        temp=head;
        for (int i = 0; i < len-n+1; i++) {
            temp=temp.next;
        }
        System.out.println("Nth node from the end of the LL :"+temp.data);
    }

    public void printMiddle(Node<Integer> head){
        Node<Integer> slow= head, fast=head;
        while (fast !=null && fast.next!=null){
            fast=fast.next.next;
            slow=slow.next;
        }
        if (slow !=null)
            System.out.println("Middle element is :"+slow.next);
    }

    public int count(Node<Integer> head, int x){
        Node<Integer> temp=head;
        int count=0;
        while (temp!=null){
            if (temp.data==x){
                count++;
            }
            temp=temp.next;
        }
        return count;
    }

    public int countRecursive(Node<Integer> head, int x){
        if (head==null)
            return 0;
        if (head.data==x)
            return 1+countRecursive(head.next,x);
        return countRecursive(head.next, x);
    }

    public boolean detectLoop(Node<Integer> head){
        HashSet<Node<Integer>> s= new HashSet<>();
        Node<Integer> temp = head;
        while (temp!=null){
            if (s.contains(temp))
                return true;
            s.add(temp);
            temp=temp.next;
        }
        return false;
    }
    //Using slow and fast pointer
    public void detectLoop2(Node<Integer> head){
        Node<Integer> slow=head, fast=head;
        int flag=0;
        while (slow!=null && fast!=null && fast.next!=null){
            slow=slow.next;
            fast=fast.next.next;

            if (slow==fast){
                flag=1;
                break;
            }
        }
        if (flag==1){
            System.out.println("Loop detected");
        }else {
            System.out.println("No loop detected");
        }

    }

    /**
     * Find Length of the loop
     *
     */

    public int countLength(Node<Integer> n){
        int res=1;
        Node<Integer> temp=n;
        while (temp.next !=n){
            res++;
            temp=temp.next;
        }
        return res;
    }

    public int countLoopLength(Node<Integer> head){
        Node<Integer> slow=head, fast=head;
        while (slow!=null && fast !=null && fast.next !=null){
            slow=slow.next;
            fast=fast.next.next;

            if (slow==fast){
                return countLength(slow);
            }
        }
        return 0;
    }



    public static void checkBounds(int position,int low, int high){
        if (position<low || position>high){
            throw new IndexOutOfBoundsException(position+"");
        }
    }

}
