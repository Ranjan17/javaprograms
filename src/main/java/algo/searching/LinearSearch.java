package algo.searching;

public class LinearSearch {

    public static int linearSearch(int[] arr, int value){
        for (int i = 0; i < arr.length; i++) {
            if (arr[i]==value){
                return i;
            }
        }
        return  -1;
    }

    public static void main(String[] args) {
        int[] arr={23,43,12,34,22,33,54,13,22,9,4};
        System.out.println(linearSearch(arr, 22));
    }
}
