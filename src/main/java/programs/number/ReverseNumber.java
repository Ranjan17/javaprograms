package programs.number;

public class ReverseNumber {

    public static int reverseNum(int num){
        int temp =num;
        int reversedNum = 0;
        while (num != 0){
            int rem = num % 10;
            reversedNum = reversedNum * 10 + rem;
            num = num /10;
        }
        return reversedNum;
    }


    public static void main(String[] args) {
        System.out.println(reverseNum(1234));
    }
}
