package exceptionHandling;

public class Example1 {
    public static void main(String[] args) {
        System.out.println(m1());
    }
    static int m1(){
        try {
            System.out.println("In outer try");
            try {
                System.out.println("In Inner try");
            }catch (ArithmeticException ae){
                System.out.println("Inner catch");
            }finally {
                System.out.println("Inner finnally");
                return 10;
            }
        }catch (NullPointerException ne){
            System.out.println("outer catch");

        }finally {
            System.out.println("Outer finnally");
        }
        System.out.println("After outer try catch finally");
        return 20;
    }

}
