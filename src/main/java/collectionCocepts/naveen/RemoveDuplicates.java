package collectionCocepts.naveen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class RemoveDuplicates {
    public static void main(String[] args) {
        ArrayList<Integer> al = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,3,4,3,45,6,5,4,5,4,3));

        LinkedHashSet<Integer> withoutDuplicate = new LinkedHashSet<>(al);
        System.out.println(withoutDuplicate);

        //JDK 8
        ArrayList<Integer> marks = new ArrayList<>(Arrays.asList(1,2,3,4,5,6,3,4,3,45,6,5,4,5,4,3));
        List<Integer> ne=marks.stream().distinct().collect(Collectors.toList());
        System.out.println(ne);
    }
}
