package algo.list;

import java.util.Scanner;

/*
public class Node<T> {
    T data;
    Node<T> next;
    public Node(T data){
        this.data=data;
    }
}
*/

 /*class LinkedList{

    public static Node<Integer> createLinkedList(){
        Node<Integer> n1= new Node<>(10);
        Node<Integer> n2= new Node<>(20);
        Node<Integer> n3= new Node<>(30);
        Node<Integer> n4= new Node<>(40);
        n1.next=n2;
        n2.next=n3;
        n3.next=n4;
        return n1;
    }

    public static void printLinkedList(Node<Integer> head){
        Node<Integer> temp= head;
        while (temp !=null){
            System.out.print(temp.data+"-->");
            temp=temp.next;
        }
        System.out.println();
    }

    public static void increment(Node<Integer> head){
        Node<Integer> temp= head;
        while (temp !=null){
            temp.data++;
            temp=temp.next;
        }
    }

     public static int lengthLinkedList(Node<Integer> head){
         int count=0;
         Node<Integer> temp=head;
         while (temp!=null){
             count++;
             temp=temp.next;
         }
         return count;
     }

     public static void printIthNode(Node<Integer> head, int i){
        int position=0;
        Node<Integer> temp=head;
        while (temp !=null && position<i){
            temp=temp.next;
            position++;
        }
        if (temp!=null){
            System.out.println(temp.data);
        }else {
            throw new ArrayIndexOutOfBoundsException();
        }
     }
//This method time complexity is O(n^2)
     public static Node<Integer> takeInput(){
         Scanner sc = new Scanner(System.in);
         int data= sc.nextInt();
         Node<Integer> head= null;
         while (data !=-1){
             Node<Integer> currentNode= new Node<>(data);
             if (head==null){
                 head=currentNode;
             }else {
                 Node<Integer> tail= head;
                 while (tail.next!=null){
                     tail=tail.next;
                 }
                 tail.next=currentNode;
             }
         }
         return head;

     }

     public static Node<Integer> takeInputOptimized(){
        Scanner sc= new Scanner(System.in);
        int data= sc.nextInt();
        Node<Integer> head = null, tail=null;
        while (data!=-1){
            Node<Integer> currentNode= new Node<>(data);
            if (head==null){
                head=currentNode;
                tail=currentNode;
            }else {
                tail.next=currentNode;
                tail=currentNode;
            }
        }
        return head;
     }


     *//**
      * Insert a node in LL
      * It can be inserted at starting ie 0th position
      * In between any two nodes
      * At tail
      * @param args
      *//*

     public static void push(Node<Integer>head, int data){
            Node<Integer> newNode= new Node<Integer>(data);
            newNode.next= head;
            head=newNode;

     }

     public static void afterNodeInsert(Node<Integer> prevNode, int data){

     }

     public static void main(String[] args) {
         *//*Node<Integer> n1= new Node<>(10);
         System.out.println(n1.data);
         System.out.println(n1.next);*//*
         Node<Integer> head= createLinkedList();
         increment(head);
         printLinkedList(head);
         System.out.println(lengthLinkedList(head));
         printIthNode(head, 10);
     }



}
*/