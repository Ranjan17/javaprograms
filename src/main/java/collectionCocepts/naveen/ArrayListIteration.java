package collectionCocepts.naveen;

import java.util.ArrayList;
import java.util.Iterator;

public class ArrayListIteration {
    public static void main(String[] args) {
        ArrayList<Integer> al = new ArrayList<>();
        al.add(10);
        al.add(40);
        al.add(30);
        al.add(20);

        //Iterating using for loop
        for (int i=0; i<al.size(); i++){
            System.out.println(al.get(i));
        }
        System.out.println("________________________");
        //Using for each loop
        for(int i : al){
            System.out.println(i);
        }
        System.out.println("________________________");
        //using streams
        al.forEach(ele-> System.out.println(ele));


        Iterator<Integer> it =al.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }

}
