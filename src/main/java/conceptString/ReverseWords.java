package conceptString;

public class ReverseWords {
    public static String reverseWords(String s){
        String[] sarray= s.split(" ");
        String result ="";
        for(int i = sarray.length-1; i>=0; i--){
            result+= sarray[i]+" ";
        }
        return result.trim();
    }

    public static void main(String[] args) {
       String x=reverseWords("How are you?");
        System.out.println(x);
    }
}
