package conceptString;

public class Test {
   public static void moveZerosToEnd(int  arr[]){
       int j=0, i;
       for (i=0; i< arr.length; i++){
           if (arr[i]!=0 && arr[j]==0){
               swaping(arr, i,j);
           }
           if (arr[j]!=0){
               j+=1;
           }
       }
   }

   public static int[] swaping(int[] ar  , int i,int j){
       int temp =ar[i];
       ar[i]=ar[j];
       ar[j]= temp;
       return ar;
   }

   public static void printArray(int[] arr){
       for (int i : arr){
           System.out.print(i+" ");
       }
   }

    public static void main(String[] args) {
        int[] a = {0,1,0,4,0,56,7,0,9,0,4,0,0};
        moveZerosToEnd(a);
        printArray(a);
    }

}
