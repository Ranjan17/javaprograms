package collectionCocepts.naveen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SynchronizedArraylist {
    public static void main(String[] args) {
        List<String> names= Collections.synchronizedList(new ArrayList<>());

        names.add("Java");
        names.add("Python");
        names.add("JS");

        //To add or remove we don't need explicit synchronization

        //While get/traverse we need explicit synchronization

        synchronized (names){
            Iterator<String> it=names.iterator();
            while (it.hasNext()){
                System.out.println(it.next());
            }
        }


        //CopyOnWriteArrayList method

        CopyOnWriteArrayList<String> empList = new CopyOnWriteArrayList<>();
        empList.add("Ram");
        empList.add("Shyam");

        // We dont need any explicit synchronization to add/ delete/ traverse

        for (String s: empList
             ) {
            System.out.println(s);

        }
    }
}
