package algo.sorting;

public class InsertionSort {
    //divide int to portion
    //First element is sorted and remaining is unsorted and


    public static void insertionSort(int [] arr){
        for (int firstUnsortedIndex=1; firstUnsortedIndex<arr.length; firstUnsortedIndex++){
            int newElement= arr[firstUnsortedIndex];
            int i;
            for (i = firstUnsortedIndex; i >0 && arr[i-1]>newElement ; i--) {
                arr[i]= arr[i-1];
            }
            arr[i]=newElement;
        }

        for (int ele : arr){
            System.out.print(ele+" ");
        }
    }



    //CodingNinja
    public static void insertionSort2(int[] arr){
        for (int i = 1; i < arr.length ; i++) {
            int temp=arr[i];
            int j=i-1;
            while (j>=0 &&arr[j]>temp){
                arr[j+1]=arr[j];
                j--;
            }
            arr[j+1]=temp;
        }

        for (int ele: arr){
            System.out.print(ele+" ");
        }
    }

    public static void main(String[] args) {
        int [] arr= {34,-22,45,12,65,-18,12,2};
        insertionSort2(arr);
    }

}
