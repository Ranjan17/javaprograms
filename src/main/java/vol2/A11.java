package vol2;

public class A11 {
    static  int a =10;
    int x = 20;
    static {
        System.out.println("Class A is loaded");
    }
}

class B11 extends A11{
    static  int b =30;
    int y = 40;

    static {
        System.out.println("Class b is loaded");
    }
}

class C11 extends B11{
    static  int c = 50;
    int z = 60;
    static {
        System.out.println("Class c is loaded");
    }
}

class TestABC11{
    public static void main(String[] args) {
        B11 b1 = new B11();
        System.out.println(b1.a);
        System.out.println(b1.x);
        b1.a =100;
        b1.x =200;
        System.out.println(b1.a);
        System.out.println(b1.x);

        C11 c1 = new C11();
        System.out.println(c1.a);
    }
}



