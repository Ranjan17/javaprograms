package logicalProgram.stringProgram;

import java.util.HashMap;
import java.util.Set;

public class DuplicateWords {

    public static void printDuplicateWords(String str){
        String [] words = str.split(" ");
        HashMap<String, Integer> wordMap = new HashMap<>();
        for (String word:words) {
            if (wordMap.containsKey(word.toLowerCase())){
                wordMap.put(word, wordMap.get(word)+1);
            }else {
                wordMap.put(word, 1);
            }
        }

        // Extracting all the Keys of the map
        Set<String> allWords = wordMap.keySet();
        for (String word : allWords){
            if(wordMap.get(word)> 1){
                System.out.println(word+" : "+wordMap.get(word));
            }
        }
    }





}
