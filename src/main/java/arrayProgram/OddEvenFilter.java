package arrayProgram;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OddEvenFilter {
    public List<Integer> evenList(ArrayList<Integer> arrayList){
        return arrayList.stream().filter(i->i%2==0).collect(Collectors.toList());
    }

    public List<Integer> oddList(ArrayList<Integer>arrayList){
        return arrayList.stream().filter(i->i%2 !=0).collect(Collectors.toList());
    }

    public List<List<Integer>> evenOddList(ArrayList<Integer>arrayList){
        List<Integer> al1= arrayList.stream().filter(i->i%2==0).collect(Collectors.toList());
        List<Integer> al2= arrayList.stream().filter(i->i%2!=0).collect(Collectors.toList());
        List<List<Integer>> l= new ArrayList();
        l.add(al1);
        l.add(al2);
        return l;
    }

}
