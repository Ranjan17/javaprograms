package vol2;

public class A13 {
    void m1(){
        System.out.println(" A13 m1()");
    }
    void m2(){
        System.out.println("A13 m2()");
        m1();
    }
}

class B13 extends A13{
    void m1(){
        System.out.println("B13 m1");
    }

    public static void main(String[] args) {
        B13 b = new B13();

       // b.m1();
        b.m2();
        System.out.println("-------------------");

        A13 a = new B13();
       // a.m1();
        a.m2();

    }
}

class A14{
    static void m1(){
        System.out.println("A m1");
    }

    void  m2(){
        System.out.println("A m2");
        m1();
    }
}

class B14 extends A14{
    static void m1(){
        System.out.println("B m1");
    }

    void m2(){
        System.out.println("B m2");
    }


    public static void main(String[] args) {
        B14 b = new B14();
       // b.m1();

        A14 a = new B14();
       // a.m1();
        a.m2();
    }

}


class A15{
    static void m1(){
        System.out.println("A m1");
    }

    static void m2(){
        System.out.println("A m2");
        m1();
    }
}

class B15 extends A15{
   static void m1(){
        System.out.println("B m1");
    }

    void m3(){
        System.out.println("B m3");
        m1();
        super.m2();
    }
}

class C15 extends B15{
    static void m2(){
        System.out.println("C m2");
        //m4();
    }
}

class D15 extends C15{
    static void m1(){
        System.out.println("D m1");
    }


    static void m2() {
        System.out.println("D m2");
    }

    static void m4(){
        System.out.println("D m4");
    }

    public static void main(String[] args) {
        D15 d = new D15();
        //d.m1();
        //d.m2();
        d.m3();
       // d.m4();

    }
}