package collectionCocepts.naveen;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayListMethod {
    public static void main(String[] args) {
        ArrayList<String> al1 = new ArrayList<>();
        al1.add("Java");
        al1.add("Python");
        al1.add("Ruby");
        al1.add("JS");

        /*ArrayList<String> al2 = new ArrayList<>();
        al2.add("Test");
        al2.add("Dev");

        al1.addAll(al2);
        System.out.println(al1);
        al1.clear();
        System.out.println(al1);*/
/*

        ArrayList<String> al3 = (ArrayList<String>) al1.clone();

        System.out.println(al3.contains("Java"));
        System.out.println(al3.contains("xyz"));
        System.out.println(al3.indexOf("Java")>= 0);

*/

        ArrayList<Integer> nums = new ArrayList<>(Arrays.asList(1,2, 3,4 , 5, 6,7 , 8, 9));
        nums.removeIf(num->num%2 ==0);
        System.out.println(nums);

        //Creating a sublist from a list

        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1,2,3,4,5,5,6,7,8,8,9,0,0,12,34,5,65));

        ArrayList<Integer> subList = new ArrayList<>(list1.subList(2,6));
        System.out.println(subList);


    }
}
