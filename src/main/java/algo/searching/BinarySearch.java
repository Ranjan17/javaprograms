package algo.searching;

/**
 * In Binary search array should be in sorted order
 */

public class BinarySearch {

    public static int iterativeBinarySearch(int[] arr, int value) {
        int start = 0;
        int end = arr.length;

        while (start < end) {
            int mid = (start + end) / 2;
            if (arr[mid] == value) {
                return mid;
            } else if (arr[mid] < value) {
                start = mid + 1;
            } else {
                end = mid;
            }

        }
        return -1;

    }


    public static int binarySearchRecursive(int[] arr, int value){
        return binarySearchRecursive(arr,0,arr.length, value);
    }


    public static int binarySearchRecursive(int[] arr,int start, int end, int value){
        if (start>=end){
            return -1;
        }
        int mid= (start+end)/2;
        if (arr[mid]==value){
            return  mid;
        }
        else if (arr[mid]<value){
           return binarySearchRecursive(arr,mid+1,end, value);
        }else {
            return binarySearchRecursive(arr,start,mid, value);
        }
    }


    public static void main(String[] args) {
        int[] arr={23,43,12,34,22,33,54,13,22,9,4};
        System.out.println(iterativeBinarySearch(arr, 212));
        System.out.println(binarySearchRecursive(arr,22));

    }

}
