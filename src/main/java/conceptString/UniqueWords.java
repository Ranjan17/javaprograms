package conceptString;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class UniqueWords {
    public static void findUniqueWords(String s){
        // Store the words in a array
        String[] words= s.split(" ");

        //Create a hasm map
        HashMap<String, Integer> map = new LinkedHashMap<String, Integer>();

        for(String word : words){
            // If word is present in the map
            if(map.containsKey(word)){
                map.put(word, map.get(word)+1);
            }
            else {
                map.put(word, 1);
            }
        }

        //Iterate over the map to extract all the keys
        for (Map.Entry<String, Integer> entry : map.entrySet()){
            if(entry.getValue()==1){
                System.out.println(entry.getKey());
            }
        }
    }

    /**
     * Write a method to print all
     * @param args
     */

    public static void main(String[] args) {
        String s= "Welcome to geeks for geeks to the world of programming";
        findUniqueWords(s);
    }
}
