package algo.list;

import java.util.Stack;

public class SinglyLinkedListUtility extends SinglyLinkedList{
    //Check linkedList is palindrome or not
    // Method-1 using Stack

    public   boolean isPalindrome_Stack(Node<Integer> head){
        Node<Integer> temp= head;
        Stack<Integer> stack= new Stack<>();
        boolean isPalindrome= true;
        //Traverse through the LL and push all Nodes to stack
        while (temp!= null){
            stack.push(temp.data);
            temp=temp.next;
        }

        temp=head;
        while (temp!=null){
            int i= stack.pop();
            isPalindrome= temp.data == i;
            temp=temp.next;
        }
        return  isPalindrome;

    }


    //Reverse a LL using iterative method
    public Node<Integer> reverse_iterative(Node<Integer> head){
        Node<Integer> prev=null, nextNode=null;
        Node<Integer> curr= head;
        while (curr !=null){
            nextNode= curr.next;
            curr.next=prev;
            prev=curr;
            curr=nextNode;
        }
        return prev;
    }

    //Reverse LL using recursive approach
    public Node<Integer> reverse_recursive(Node<Integer> head){
        Node<Integer> first, rest;
        //Empty list base case
        if (head==null)
            return null;
        first=head;      //Let first= {1,2,3}
        rest= first.next; //rest= {2,3}

        //Empty rest base case
        if (rest==null)
            return head;
        rest= reverse_recursive(rest);
        first.next.next=first;
        first.next=null;
        head=rest;

        return head;


    }
}






