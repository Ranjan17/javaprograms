package exceptionHandling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Division {
    public static void div(){
        try{
            BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
            int a =-1;
            int b =-1;
            while (true){
                try{
                    System.out.println("Enter first number");
                    a = Integer.parseInt(br.readLine());
                    break;
                }catch (NumberFormatException ne){
                    System.out.println("wrong input enter only numbers");
                }
            }
            while (true){
                try {
                    System.out.println("Enter second number");
                    b = Integer.parseInt(br.readLine());
                    try {
                        int c = a/b;
                        System.out.println("Result= "+c);
                    }
                    catch (ArithmeticException ae){
                        System.out.println("Worng input dont pass zero for second input");
                        continue;
                    }
                    break;
                }
                catch (NumberFormatException ne){
                    System.out.println("Wrong inputs , enter only numbers");
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
