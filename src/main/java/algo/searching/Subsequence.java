package algo.searching;

public class Subsequence {

    /**
     * Str1 = wearedubaiexpo
     * str2= wrdaxo
     */

    public static boolean isSubsequence(String s1, String s2){
       int j=0;
       int m= s1.length();
       int n= s2.length();
        for (int i = 0; i < n && j<m; i++) {
            if (s1.charAt(j)==s2.charAt(i)){
                j++;
            }

        }
        return j==m;
    }

}
