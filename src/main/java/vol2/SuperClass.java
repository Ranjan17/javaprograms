package vol2;

public class SuperClass {
    SuperClass(){
        this(10);
        System.out.println("Super class no arg");
    }
    SuperClass(int a){
        this("abc");
        System.out.println("Super class int arg");
    }
    SuperClass(String s){
        System.out.println("Super class string arg");
    }

}

class SubClass extends SuperClass{
    SubClass(){
        this(19);
        System.out.println("Sub class no arg");

    }
    SubClass(int a){
        this("abc");
        System.out.println("Subclass int arg");
    }

    SubClass(String s){
        System.out.println("Subclass string arg");
    }
}

class TestDemo{
    public static void main(String[] args) {
        new SubClass();
        /*System.out.println();
        new SubClass(10);
        System.out.println();
        new SubClass("abc");*/
    }
}