package seleniumUtils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverBasics {
    public WebDriver driver=null;
    public void launchBrowser(){
        System.setProperty("webdriver.chrome.driver","path");
        WebDriver driver= new ChromeDriver();
        driver.get("url");
    }

    public WebDriver initDriver(String browserName){
        if (browserName.equalsIgnoreCase("chrome")){
            WebDriverManager.chromedriver().setup();
            driver=new ChromeDriver();
        }
        else if (browserName.equalsIgnoreCase("firefox")){
            WebDriverManager.firefoxdriver().setup();
            driver= new FirefoxDriver();
        }
        return driver;
    }

    public void launchUrl(String url){
        if (url!=null || !(url.isEmpty())){
            driver.get(url);
        }

    }
}
