package collectionCocepts.naveen;

public class MyLinkedList {
    Node head;
    class Node{
        int data;
        Node next;

        Node(int data){
            this.data = data;
            next= null;
        }

    }

    public static void main(String[] args) {
        MyLinkedList ll = new MyLinkedList();
        Node first = ll.new Node(10);
        ll.head =first;
    }
}
