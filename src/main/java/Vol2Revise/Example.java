package Vol2Revise;

public class Example {

    /*Example(){
        System.out.println("EX No arg constructor");
    }*/

    Example(int a){
        System.out.println("Ex Arg constructor ");
    }
}

class Sample extends Example{
   /* Sample(){
        System.out.println("SA no arg");

    }*/
    Sample(int a){
        super(50);
        System.out.println("Sa arg const");
    }

    public static void main(String[] args) {
        Sample s1 = new Sample(10);
    }
}
