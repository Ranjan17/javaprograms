package conceptString;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class BasicPrograms {
    //Find duplicate words of the string

    public static  void duplicateWords(String s){
        String[] words= s.split(" ");
        HashMap<String, Integer> wordMap= new HashMap<String, Integer>();
        for (String word : words){
            if(wordMap.containsKey(word)){
                wordMap.put(word,wordMap.get(word)+1);
            }else{
                wordMap.put(word, 1);
            }
        }

        for(Map.Entry<String, Integer> entry : wordMap.entrySet()){
            if(entry.getValue()!= 1){
                System.out.println(entry.getKey());
            }
        }

    }

    //Alternate method to find unique words in  a string

    public static void uniqueWords(String s){
        String[] words = s.split(" ");
        HashMap<String, Integer> map = new LinkedHashMap<String, Integer>();

        for(String word : words){
            if(map.containsKey(word)){
                map.put(word, map.get(word)+1);
            }else{
                map.put(word, 1);
            }
        }

        //EXtract all the keys of the map
        Set<String> wodsInString = map.keySet();

        for(String word : wodsInString){
            if(map.get(word)==1){
                System.out.println(word);
            }
        }
    }

    //Count number of words in a string
    public static int wordCount(String s){
        String[] words = s.trim().split(" ");
        return words.length;

    }

    //Alternate method to calculate word count of a string

    public static int wordCount2(String s){
        int count=1;
        for(int i=0; i<s.length()-1; i++){
            if((s.charAt(i)==' ') && (s.charAt(i+1) !=' ')){
                count++;
            }
        }
        return count;
    }

    //Count total number of occurances of a character

    public static int characterOccurances(String s, char ch){
        int count=0;
        for(int i=0; i<s.length()-1; i++){
            if(s.charAt(i)==ch){
                count++;
            }
        }
        return count;
    }

    /**
     * Reverse a String
     * @param s
     */

    public static void reverseString(String s){
        String result="";
        for(int i=s.length()-1; i>=0; i--){
            result += s.charAt(i);
        }
        System.out.println(result);
    }

    //Method 2 using char array

    public static  void reverse2(String s){
        char[] ch = s.toCharArray();
        for(int i= s.length()-1; i>=0; i--){
            System.out.print(ch[i]);
        }
    }

    //Method 3 using StrigBuffer
    public static void reverse3(String s){
        StringBuffer sb = new StringBuffer(s);
        sb.reverse();
        System.out.println(sb);

    }

    /**
     * Revesre each ord of the string
     * @param str
     */
    public static void reverseWords(String str){
        String[] words= str.split(" ");
        String result = "";
        for(String word : words){
            char[] ch = word.toCharArray();
            String reversedWord= "";
            for (int i = ch.length-1; i>=0; i--){
                reversedWord = reversedWord + ch[i];
            }
            result = result+ reversedWord+" ";
        }
        System.out.println(result.trim());
    }





    public static void main(String[] args) {
        String st= "hello world hello I am the Strongest man in the world";
      //  duplicateWords(st);
       // uniqueWords(st);
      //  String  s="hello world";
       // reverseString(s);
      //  reverseWords(s);
        System.out.println(st.substring(2,4));

    }
}
