package algo.sorting;

public class BubbleSort {

    public static void bubbleSort(int[] arr){
        for (int i=0; i<arr.length;i++){
            for (int j=i+1; j<arr.length; j++){
                if (arr[i]>arr[j]){
                    swap(arr,i,j);
                }
            }
        }

        for (int a: arr){
            System.out.print(a+" ");
        }
    }

    public static void swap(int[] arr, int i, int j){
        if(i==j){
            return;
        }
        int temp= arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
    }

    public static void main(String[] args) {
        int[] a= {6,4,8,3,78,23,56,22};
        bubbleSort(a);
    }
}
