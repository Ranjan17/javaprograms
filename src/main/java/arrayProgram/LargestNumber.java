package arrayProgram;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class LargestNumber {
    public static int largestNumber(int[] arr){
        int largestNum=arr[0];
        for (int i=1; i<arr.length; i++){
            if (arr[i]>largestNum){
                largestNum=arr[i];
            }
        }
        return largestNum;
    }

    public static int largestNumber2(int[] arr){
        return Arrays.stream(arr).max().getAsInt();
    }



    public static void main(String[] args) {
        int[] a= {12,43,13,34,54,67,45};
        System.out.println(largestNumber2(a));


    }
}
