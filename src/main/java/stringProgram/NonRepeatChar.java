package stringProgram;

import java.util.HashMap;
import java.util.Map;

public class NonRepeatChar {

    public static void main(String[] args) {
        System.out.println( firstNonRepeatingChar3("aaabcccdeef"));
    }

    public static char firstNonRepeatCharacter1(String s){
        for (int i=0; i<s.length(); i++){
            boolean seenDuplicate= false;
            for (int j=0; j<s.length();j++){
                if (s.charAt(i)==s.charAt(j) &&(i!=j)){
                    seenDuplicate=true;
                    break;
                }
            }
            if (!seenDuplicate){
                return s.charAt(i);
            }
        }
        return '_';
    }

    public static char firstNonRepeatingChar2(String s){
        Map<Character, Integer> char_map= new HashMap<>();
        for (int i=0; i<s.length(); i++){
            char c= s.charAt(i);
            if (char_map.containsKey(c)){
                char_map.put(c, char_map.get(c)+1);
            }else {
                char_map.put(c,1);
            }
        }
        for (int i=0; i<s.length(); i++){
            char c= s.charAt(i);
            if (char_map.get(c)==1){
                return c;
            }
        }
        return '_';
    }

    public static char firstNonRepeatingChar3(String s){
        for (int i=0; i<s.length();i++){
            if (s.indexOf(s.charAt(i))== s.lastIndexOf(s.charAt(i)))
                return s.charAt(i);
        }
        return '_';
    }
}
