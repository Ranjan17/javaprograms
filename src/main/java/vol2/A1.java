package vol2;

public class A1 {
    static int a =m1();
    public static int m1(){
        System.out.println("M1 called");
        return 10;
    }
    static {
        System.out.println("SB called and A class loaded");
    }

    public static void main(String[] args) {
        System.out.println("A main");
    }
}
