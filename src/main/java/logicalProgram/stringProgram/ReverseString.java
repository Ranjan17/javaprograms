package logicalProgram.stringProgram;

public class ReverseString {

    public static void reverseString(String str){
        char[] ch = str.toCharArray();
        for (int i=ch.length-1; i>=0; i--){
            System.out.print(ch[i]);
        }
    }

    public static void reverse2(String str){
        StringBuilder sb = new StringBuilder(str);
        System.out.println(sb.reverse());
    }

    public static void reverse3(String str){
        StringBuffer sbr = new StringBuffer(str);
        System.out.println(sbr.reverse());
    }



    public static void main(String[] args) {
        reverseString("Hello");
    }

}
