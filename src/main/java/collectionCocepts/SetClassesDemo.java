package collectionCocepts;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetClassesDemo {
    public static void main(String[] args) {
        //Set object creation
        HashSet hs = new HashSet();
        LinkedHashSet lhs = new LinkedHashSet();
        TreeSet ts = new TreeSet();

        //Print the initial size
        /*System.out.println("hs:->"+hs.size());
        System.out.println("lhs:->"+lhs.size());
        System.out.println("ts:->"+ts.size());*/

        //Adding elenments to hs
        System.out.println("is 20 added  "+hs.add(Integer.toString(20)));
        System.out.println("is a added  "+hs.add('a'));
        System.out.println("is b added  "+hs.add('b'));
        System.out.println("is \"abc\" added  "+hs.add("abc"));
        System.out.println("is \"Abc\" added  "+hs.add("Abc"));
        System.out.println("is \"abc\" added  "+hs.add("abc"));
        System.out.println("is String \"abc\" added  "+hs.add(new String("abc")));
        System.out.println("is Ex added  "+hs.add(new Example()));
        System.out.println("is EX added  "+hs.add(new Example()));


        System.out.println(hs.size());
        System.out.println(hs);
    }
}

class Example{
    int x = 10;
    int y = 30;
}