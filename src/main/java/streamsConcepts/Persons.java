package streamsConcepts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Persons {

    public static void main(String[] args) {
        List<String> l = new ArrayList<>();
        List<String> collect = l.stream().filter(s -> s.length() >= 5)
                .limit(10)
                .collect(Collectors.toList());
        collect.forEach(System.out::println);
    }

    public void range(){
        for (int i=0; i<=10; i++){
            System.out.println(i);
        }
        IntStream.range(0,10).forEach(System.out::println);
        IntStream.rangeClosed(0,10).forEach(System.out::println);
    }


    public void  min(){
      final   List<Integer> l= Arrays.asList(6,4,7,3,68,5,7);
        Integer min = l.stream().min(Integer::compareTo).get();
        System.out.println(min);
    }
}
