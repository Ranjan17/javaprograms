package conceptString;

public class StringOperations {
    public static void main(String[] args) {
        String s = "Java pogramming Language";
        // Checking String is empty  or not
       // System.out.println(s.isEmpty());

        //Finding length of the String
       // System.out.println(s.length());

//        String s1="";
//        String s2= " ";
//        String s3 = new String(" ");
//        System.out.println(s1.isEmpty());
//        System.out.println(s2.isEmpty());
//        System.out.println(s3.isEmpty());
//
//        System.out.println(s1.length());
//        System.out.println(s2.length());
//        System.out.println(s3.length());
//        System.out.println(s);

        //Comparing two Strings
//        String s1 = new String("abc");
//        String s2 = new String("abc");
//        String s3 = new String("Abc");
//
//        System.out.println(s1==s2);
//        System.out.println(s1.equals(s2));
//        System.out.println(s1.equalsIgnoreCase(s3));

        System.out.println(s.startsWith("java"));
        //Character at specific index
        System.out.println(s.charAt(10));

        //Print all characters in a given string with index
        /*for(int i=0; i< s.length(); i++){
            System.out.println(i+"->"+ s.charAt(i));
        }*/

       // System.out.println(s.substring(24,24));

        /*String s2= "Ha Ha Ha Ha";
        System.out.println(s2.replace("Ha", "Hello"));
        */

        String s4="Hello world";
        String s5 = s4.trim();
        System.out.println(s4==s5);


    }
}
